Extending the Library
=====================

* Currently, you can only create a "visual novel" type of game if you use TreeStory exclusively.
* If you want to use TreeStory for a different game type, or if you just want to improve upon what is currently available, then you can extend the TreeStory library using your own scripts.
* TreeStory can be extended easily and anything that you add to it will automatically work with the provided editor.

.. toctree::
    :caption: Custom Scripts
    :maxdepth: 1

    extending-the-library/custom-game-data
    extending-the-library/custom-library
    extending-the-library/custom-library-editor
    extending-the-library/custom-node
    extending-the-library/custom-node-editor
    extending-the-library/custom-event-handler
    extending-the-library/custom-event-handler-editor
