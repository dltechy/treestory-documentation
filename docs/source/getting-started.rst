Getting Started
===============

The main component of TreeStory is a node library. Node libraries contain the flow of the game you are going to make.

.. figure:: /_images/library-icons/node-library-icon.png
    :align: center
    :alt: node library icon

    The Node Library

Creating a node library
-----------------------

1. Either right-click on the project window or click on "Assets" in Unity's menu bar.
2. Go to Create -> TreeStory -> Node Library.
3. This will create a new node library asset.

Opening the TreeStory Editor
----------------------------

Either:
    * Double click the created node library asset.
    * Click on Window -> TreeStory Editor in Unity's menu bar.

.. figure:: /_images/treestory-windows/treestory-editor.png
    :align: center
    :alt: treestory editor

    The TreeStory Editor
