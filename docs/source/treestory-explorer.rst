The TreeStory Explorer
======================

When your project grows to have a lot of libraries, it can be difficult to manage them. With that in mind, an explorer window has been provided.

* The explorer groups all library files into their own list.
* Entries for each list are arranged alphabetically.
* Clicking on an entry automatically selects your library asset as if you clicked on it in the project hierarchy window.
* Clicking on an entry also automatically opens the TreeStory editor.

Opening the TreeStory Explorer
------------------------------

1. Click on "Window" in Unity's menu bar.
2. Select "TreeStory Explorer".

.. figure:: _images/treestory-windows/treestory-explorer.png
    :align: center
    :alt: the treestory explorer

    The TreeStory Explorer

Searching for libraries
-----------------------

1. Locate the search bar on top of the TreeStory Explorer window.
2. Type the name of the library that you want to find.
