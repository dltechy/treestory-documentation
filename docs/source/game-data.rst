Creating Game Data
==================

To use your own assets in TreeStory, you first need to convert them into useable game data. This can be done by using TreeStory's custom libraries which stores game data like actors, locations, audio and video. Each library is stored in its own asset file. Creating any TreeStory library is similar to how you create a node library shown previously.

Creating a library
------------------

1. Either right-click on the project window or click on "Assets" in Unity's menu bar.
2. Go to Create -> TreeStory.
3. Select which library you want to create.

* Due to the nature of how TreeStory handles the saving and loading of files, you must place ALL libraries inside any folder names "Resources" to be able to utilize TreeStory's save system.
* You can create multiple libraries of each type. TreeStory is able to find any game data even if they are separated in multiple library assets. This is useful for organization of your project especially if your project grows too large. This is also useful if you work in a group, since each group member can work on separate libraries.

Editing library entries
-----------------------

* Select the library asset that you want to edit from Unity's hierarchy window.

    - This will show you a data list inside Unity's inspector window (The image below shows the actor data list as an example).

.. figure:: /_images/actor-library/actor-data-list-empty.png
    :align: center
    :alt: actor data list (empty)

    Actor Data List (Empty)

* Press the (+) button on the lower right of the data list to create a new entry to the library.
* Press the (-) button on the lower right of the data list to remove the selected entry from the library.

    - Alternatively, you can press the delete key on your keyboard.
    - You can also right click on the library entry and press the "Delete" button from the menu that pops up.

* When you have multiple entries in this list, you can drag them around to rearrange them.

Searching through library entries
---------------------------------

1. Select the library that you want to search for entries.
2. Locate the search bar on top of the inspector window.
3. Type the name of the library entry/entries that you want to find.

* **Note:** Some libraries implements searching by category.

    - If this is the case, the search bar will have a label to its left and a dropdown indicator in its icon.
    - Click on either the label or the icon to reveal a dropdown menu which shows all categories to limit the search to.

Library specific creation
-------------------------

Every game data has their own properties. Because of that, each data type has its own way of creating its entries. Each one is discussed in its own section in this document.

.. toctree::
    :maxdepth: 1

    game-data/actor-data
    game-data/location-data
    game-data/variable-data
