Using the TreeStory Editor
==========================

In TreeStory, nodes are what makes your game work. It controls everything that your users will see and hear in your game. Because of that, you are most likely going to spend most of your time in the TreeStory editor, working on these nodes. Below are what you will need to start working on the nodes for your game.

Moving the editor view
----------------------

* You can move around the editor view by clicking with the middle mouse button and dragging the mouse.
* You can also press and hold the alt-key from your keyboard and dragging with the left mouse button.
* Your pan position in the editor is indicated in the toolbar on top of the editor.

    - This indicator is editable and you can use it to jump quickly to different parts of your node library.
    - The values shown here are the grid position.

.. figure:: /_images/treestory-windows/editor-pan-location.png
    :align: center
    :alt: editor pan location

    Editor Pan Location

Resetting the editor view
-------------------------

1. Right-click on an empty space on the editor window.
2. Select "Reset View" from the context menu.

Adding a node
-------------

1. Right-click on an existing node or on an empty space.
2. Select any of the add node options from the context menu.

* If you right-clicked on an existing node, the new node will automatically be the target of that node.
* If you right-clicked on an empty space, the new node will just be floating and won't be used in-game.
* Node targets can be set at any time so you don't need to worry if you made a mistake.

Deleting a node
---------------

1. Right-click on the node that you want to delete.
2. Select "Delete" from the context menu.

    * Alternatively, you can just select the node and press the delete key from your keyboard.

Moving nodes
------------

You can move your selected nodes around the editor by dragging them using the left mouse button.

Setting node targets
--------------------

1. Right-click on the node that you want to set as the parent.
2. Select "Set Target" from the context menu.

    * You should see an arrow from the parent node following your cursor.

3. Select the node that you want to set as its target.

    * If you want to cancel setting of targets, you can click on an empty space on the editor window.

.. figure:: /_images/node-links/set-node-target.png
    :align: center
    :alt: set node target

    Set Node Target
    
* Each node can only have one target node except for the dialog node which can have multiple target "choice" nodes.
* If you set a new target for a node that already has a target, then the new target will override the old one. 
* If a dialog node targets choice nodes AND a different type of node, it will prioritize the choice nodes and disregard the other target node.

Unsetting node targets
----------------------

1. Right-click on the node that you want to remove the targets.
2. Select "Unset Targets" from the context menu.

    * If you unset the targets of a dialog node which has at least one choice node as its target, you should see a red arrow from the dialog node following your cursor.

3. If you have a red arrow following your cursor, select the choice node that you want to unset as a target.

    * If you want to cancel unsetting of targets, you can click on an empty space on the editor window.

.. figure:: /_images/node-links/unset-node-target-choice-node.png
    :align: center
    :alt: unset node target choice node

    Unset Node Target (Choice Node)

Selecting multiple nodes
------------------------

Method 1:
    1. Hold the control or shift key on your keyboard.
    2. Select an unselected node to add it to the selection.

        * Selecting an already selected node removes it from the selection.

Method 2:
    1. (**Optional**) Hold the control or shift key on your keyboard.

        * This will add the contents of the selection box to the current selection.

    2. Click on an empty space on the editor window.
    3. Drag over all the nodes that you want to select.

.. figure:: /_images/node-links/node-selection-box.png
    :align: center
    :alt: node selection box

    Node Selection Box

Notes
-----

* Selected nodes have WHITE borders around them.
* End nodes have YELLOW borders around them.

    - End nodes are nodes that do not have a target.
    - They are the end points to your game.
