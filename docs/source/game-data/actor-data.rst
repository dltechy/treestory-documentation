Creating Actor Data
===================

**Note:** This section contains data relevant only to the provided TreeStoryVN package. If you plan to create actors for a different game genre, or if you want to create a different implementation for your game actors, please proceed to the "Extending the Library" section of this document.

.. figure:: /_images/library-icons/actor-library-icon.png
    :align: center
    :alt: actor library icon

    The Actor Library

TreeStoryVN actor data properties
---------------------------------

* Actor name
    - The name of the actor.
* Actor name color
    - The color that is used for the actor name text that is shown inside the game.
* Actor sprites
    - The sprites that are used to show the actor inside the game.
    - Multiple sprites can be attached to a single actor.
    
        + This can be useful if you want to add multiple poses for each of them.

The image below shows the actor data list with 3 actor entries, with their names filled-up and colors selected.

.. figure:: /_images/actor-library/actor-data-list-multiple-entries.png
    :align: center
    :alt: actor data list (multiple entries)

    Actor Data List (Multiple Entries)

Editing the actor name
----------------------

1. Click on the actor entry that you want to edit.
2. Click on the actor entry again to reveal a textbox.
3. Edit this textbox to set the actor name.

Editing the actor name color
----------------------------

1. Select the color picker at the right of the actor entry that you want to edit.
2. Select the color that you want from the window that pops up.

Editing actor sprites
---------------------

1. Select the actor entry that you want to attach sprites to.

    * This will show you another list below the actor data list.
    * This is the actor sprites list and this is what you will use to attach sprites to the selected actor.

.. figure:: /_images/actor-library/actor-sprites-list-empty.png
    :align: center
    :alt: actor sprites list (empty)

    Actor Sprites List (Empty)

2. Press the (+) button on the lower right of the actor sprites list to create a new sprite entry to the selected actor.

    * You should see an empty sprite object.

.. figure:: /_images/actor-library/actor-sprites-list.png
    :align: center
    :alt: actor sprites list (empty)

    Actor Sprites List

3. Drag your actor asset from the hierarchy window into this field to attach it to the selected actor.

    * Alternatively, you can use the select button on the lower right of the sprite field.

* Press the (-) button on the lower right of the actor sprites list to remove the selected sprite from selected actor.
* As with any data list, you can also drag actor sprites around inside this list to rearrange them.
