Creating Location Data
======================

**Note:** This section contains data relevant only to the provided TreeStoryVN package. If you plan to create locations for a different game genre, or if you want to create a different implementation for your game actors, please proceed to the "Extending the Library" section of this document.

.. figure:: /_images/library-icons/location-library-icon.png
    :align: center
    :alt: location library icon

    The Location Library

TreeStoryVN location data properties
------------------------------------

* Location name
    - The name of the location.
* Location sprite
    - The sprite that is used as the background of the game for the corresponding location.
    - Unlike actors, you can only attach one sprite to a location.

The image below shows the location data list with 3 location entries, with their names filled-up.

.. figure:: /_images/location-library/location-data-list-multiple-entries.png
    :align: center
    :alt: location data list (multiple entries)

    Location Data List (Multiple Entries)

Editing the location name
-------------------------

1. Click on the location entry that you want to edit.
2. Click on the location entry again to reveal a textbox.
3. Edit this textbox to set the location name.

Editing the location sprite
---------------------------

1. Locate the sprite field to the right of the location entry that you want to edit.
2. Drag your location asset from the hierarchy window into this field to attach it to the selected location.

    * Alternatively, you can use the select button on the lower right of the sprite field.
