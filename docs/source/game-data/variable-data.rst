Creating Variable Data
======================

.. figure:: /_images/library-icons/variable-library-icon.png
    :align: center
    :alt: variable library icon

    The Location Library

Creating a new variable entry
-----------------------------

* Unlike other libraries which you only need to click the (+) button to create a new entry, clicking the button instead opens a popup menu.
* This popup menu shows all available variable types that you can create.
* Select the type that you want to add to create a new variable entry of the selected type.

.. figure:: /_images/variable-library/create-new-variable-data.png
    :align: center
    :alt: create new variable data

    Creating A New Variable Data

Changing the variable type
--------------------------

1. Right click on the variable entry that you want to change.
2. Go to the "Change Type" sub-menu.
3. Select the type that you want to change to.

.. figure:: /_images/variable-library/change-variable-type.png
    :align: center
    :alt: change variable type

    Changing The Variable Type

TreeStory variable data properties
----------------------------------

* Variable name
    - The name of the variable.
* Variable value
    - The value of the variable.
* Variable type identifier
    - A text identifier to enable you to differentiate the type of each variable entry inside the library.
    - Values:
        + Int (I)
        + Float (F)
        + Long (L)
        + Double (D)
        + Bool (B)
        + String (S)
        + Vector2 (V2)
        + Vector3 (V3)
        + Vector4 (V4)
        + Rect (R)
        + Quaternion (Q)
        + Color (C)

The image below shows the variable data list with all types of variable entries, with their names and values filled-up.

.. figure:: /_images/variable-library/variable-data-list-multiple-entries.png
    :align: center
    :alt: variable data list (multiple entries)

    Variable Data List (Multiple Entries)

Editing the variable name
-------------------------

1. Click on the variable entry that you want to edit.
2. Click on the variable entry again to reveal a textbox.
3. Edit this textbox to set the variable name.

Editing the variable value
--------------------------

* Each variable type have a different way of editing them.
* Most of them are just simple edittable fields at the right of its entry in the inspector.
* For complex types, buttons labelled "**Edit...**" are placed instead of a simple field.

    - Clicking this button shows a popup window that contains the fields which you could edit to change the variable's value.

* Simple types:
    - Int
    - Float
    - Long
    - Double
    - Bool
    - String
    - Color
* Complex types:
    - Vector2
    - Vector3
    - Vector4
    - Rect
    - Quaternion
