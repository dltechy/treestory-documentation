Editing Nodes
=============

Editing nodes are done in Unity's inspector window. Selecting a node reveals options in the inspector window. Editing multiple nodes are possible if they are all the same type.

Common node properties
----------------------

* Node type name
    - The name of the selected nodes' type. Used only for identification.

* Wait to finish
    - Only some nodes have this property editable since the rest of the nodes will function the same way no matter the value of this property. 
    - If CHECKED, the game will wait for this node to finish its tasks before moving on to the next node.
    - If UNCHECKED, then the node's link in the editor window will change from a single white line to a double yellow line to represent it better.

.. figure:: /_images/node-links/instant-node-link.png
    :align: center
    :alt: instant node link

    Instant Node Link

* Data list
    - Some types of nodes can accept data to make them more dynamic.
    - Each node may accept different data types. These will be discussed in their own sub-section for each applicable node. 
    - The index for each data entry is shown at the left of each entry in the data list for your reference.
    - The data type for each entry in the list is shown beside its index.
    - You can add your own data to this by extending the library (discussed later).

Selecting the next node
-----------------------

* You can select the next node that the one you are currently editing is pointing to by pressing the button at the top right of the inspector window labelled "**Select Next Node**".
* This button only appears if the current node has a child node that it accepts only one of.
* If the current node accepts more than one of its child nodes, like the choice nodes of the dialog node, then selecting the child node depends on the implementation of the nodes children within its own inspector.

    - In the case of the choice nodes of the dialog node, buttons labelled "**Select**" are placed to the right of each choice entry in the "Choices" reorderable list inside the dialog node's inspector.

Editing the Data List
---------------------
1. Press the (+) button on the lower right of the data list to create a new data entry to the current node.

.. figure:: /_images/node-data-list/node-data-list-multiple-entries.png
    :align: center
    :alt: node data list (multiple entries)

    Node Data List (Multiple Entries)

2. Click on the dropdown list of the data entry that you want to edit. 
3. Select the type of library which contains the data.

    * This option only appears if the node accepts multiple data types.
    * Pressing the "[None]" option will remove the data associated with the current entry.

4. Select the library where the data is located.

    * This option only appears if you have multiple libraries of the selected type.

5. Select the data that you want to use.

* Press the (-) button on the lower right of the data list to remove the selected data from the node.

    - If there is data currently in the selected entry, pressing the (-) button will remove it, similar to pressing the "[None]" option.
    - Pressing it a second time will remove the entry altogether.

* As with any data list, you can also drag entries around inside this list to rearrange them.

.. toctree::
    :caption: The Nodes
    :maxdepth: 1

    nodes/start-node
    nodes/dialog-node
    nodes/choice-node
    nodes/actor-node
    nodes/location-node
    nodes/audio-node
    nodes/video-node
    nodes/library-node
    nodes/scene-node
    nodes/save-node
    nodes/load-node
    nodes/quit-node
