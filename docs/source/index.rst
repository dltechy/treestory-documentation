TreeStory: A Game Creation Tool for Story-Based Games Using Unity
=================================================================

Scripting is a huge part of game development in Unity. But it can also be very time consuming even for the most basic of games. To enable game developers to focus on the more important parts of actually making games, packages can be obtained from Unity's asset store which should handle all of the tedious parts of game development. One such package is TreeStory.

TreeStory is a Unity add-on to make story-based game creation as simple as possible. It uses a node editor to create dialog and events, and prefab templates to create the actual game and UI. Branching of dialog and events are also made much simpler using this tool.

Using TreeStory removes most, if not all, of the hassle of scripting for your game, enabling you to focus on more important things, like creating the game's actual story, creating game assets, and more. And even if what you need is not yet implemented, it is very easy to do so yourself, and it's still not as much of a hassle as when you start scripting from scratch.

.. toctree::
    :caption: Table of Contents
    :maxdepth: 3

    getting-started
    game-data
    treestory-explorer
    treestory-editor
    nodes
    prefabs
    creating-a-simple-visual-novel
    extending-the-library
