TreeStoryVN Prefabs
===================

* The TreeStoryVN package provides you with prefabs to help you make your game faster and easier.
* The prefabs are controlled by the nodes that you created in the node library.

    - You can opt not to add specific prefabs if they have no node counterpart in the node library.

        + **Example:** If you don't have any video node in the node library, then you don't need to add a video player prefab to your game.

* Each prefab works independently from each other.

    - You can add or remove each game object without worrying that you'll break another.

* The order of prefabs inside your game doesn't matter (Except for the drawing order).

    - The only requirement is that all prefabs should be inside a parent object which contains a "TreeStoryVN" script.

* If you want to use more than one of a specific prefab to your scene, you have to provide additional scripts to make them work.

    - **Example:** If you want to have 2 name boxes for multiple actors at the same time, then you need to provide your own script.
    - An exception for this are the regular and choice buttons as TreeStory can use an unlimited number of those.

.. toctree::
    :caption: The Prefabs
    :maxdepth: 1
    
    prefabs/main-camera-prefab
    prefabs/event-system-prefab
    prefabs/save-system-prefab
    prefabs/audio-manager-prefab
    prefabs/background-prefab
    prefabs/video-player-prefab
    prefabs/fade-image-prefab
    prefabs/actor-drawer-prefab
    prefabs/name-box-prefab
    prefabs/dialog-box-prefab
    prefabs/choice-layer-prefab
    prefabs/choice-prefab
    prefabs/button-prefab
    prefabs/mouse-button-handler-prefab
    prefabs/keyboard-handler-prefab
