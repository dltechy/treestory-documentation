Creating the Main Game
======================

Steps
-----

* Drag the "Game" prefab into the "GameLayer" object in Unity's hierarchy window.

    - Location::

        TreeStory/Prefabs/Templates/Game

    - You only place it in the GameLayer so that draw order of the game components are between the background and the overlay components.
    - If you know what you are doing, you can place the game prefab however you want inside the hierarchy.

        + Be careful about how Unity saves ordering for objects inserted between the objects of prefab instances inside the hierarchy.

.. figure:: /_images/template-prefabs/game-template-prefab.png
    :align: center
    :alt: game template prefab

    The Game Template Prefab

* Customize each game object to your liking.

    - Details on how to do that are discussed previously in each prefab's sub-section.

* The color property of the source image component for the DialogBox, NameBox, and ChoiceLayer game objects are set to transparent by default in the game template prefab.

    - Change the image component's color to white (or any color that you prefer) to make its background visible.
