Creating a Menu
===============

* Menus in TreeStory are usually implemented by creating new scenes for each.
* Using this method, you can make the menu shown by calling it using "scene nodes" inside your node libraries.
* You can either make it as a standalone scene or as an overlay to your game depending on the "load scene mode" property of the scene node that calls it.

    - A standalone menu is shown by setting it to "single".
    - An overlay is shown by setting it to "additive".
 
Steps
-----

* Drag the "Menu" prefab into the "GameLayer" object in Unity's hierarchy window.
    - Location::

        TreeStory/Prefabs/Templates/Menu

    - You only place it in the GameLayer so that draw order of the game components are between the background and the overlay components.
    - If you know what you are doing, you can place the menu prefab however you want inside the hierarchy.

        + Be careful about how Unity saves ordering for objects inserted between the objects of prefab instances inside the hierarchy.

.. figure:: /_images/template-prefabs/menu-template-prefab.png
    :align: center
    :alt: menu template prefab

    The Menu Template Prefab
    
* Customize each game object to your liking.

    - Details on how to do that are discussed previously in each prefab's sub-section.

* The color property of the source image component for the ChoiceLayer game objects are set to transparent by default in the menu template prefab.

    - Change the image component's color to white (or any color that you prefer) to make its background visible.
