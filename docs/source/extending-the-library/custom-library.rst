Creating a Custom Library
=========================

* Libraries are basically just classes which store a list of game data.
* TreeStory uses these libraries to find game data and use it inside your game.
* As such, your custom game data is useless until you create a library to store it.

Template
--------

::

    using System;
    using System.Collections.Generic;

    // Add using statements here as necessary.

    // You can also use your own namespace, just be careful of C#'s namespace rules.
    namespace TreeStory
    {
        [Serializable]
        // Add more attributes here.
        public class /* CustomLibrary */ : BaseLibrary
        {
    #if UNITY_EDITOR
            public static List</* CustomLibrary */> AllLibraries = new List</* CustomLibrary */>();
    #endif

            // This is a list variable for the custom data that you have created in the previous section.
            public List</* CustomData */> DataList = new List</* CustomData */> = new List</* CustomData */>();

            // Add variables for your class here.
            
            // Add methods for your class here.
        }
    }

Usable attributes
-----------------

* DataOf
    - You can add this attribute to link this library's game data to the specified nodes.
    - These nodes will allow this library's game data to its data list property.
    - This attribute does the exact same function as the "**DataType**" attribute for the node classes.
    
        + You can use either, but they both exist in case you can't add attributes to one or the other.

Usable methods
--------------

* public static void CreateDataDropdown
    - Use this method to add a dropdown field which contains all libraries of the specified type.
    - This method is public and can be used for other classes, like the custom node editors.
    - Parameters:
        + dataProperty (SerializedProperty)
            * The serialized property of the data that you want to edit.
        + allLibraries (IList)
            * The list of all libraries for the type that you want to use.
            * Example::
            
                VideoNode.AllLibraries

        + showNone (bool)
            * Set this to true if you want to enable the "none" option for this field in the editor.
        + tooltip (string)
            * The tooltip that will be displayed when you hover your mouse over the property inside Unity's inspector window.
            * Leave this blank or set it to an empty string to not display a tooltip.
* public static void AddMenuItems
    - Use this method to add all libraries of the specified type to a Unity generic menu.
    - This method is public and can be used for other classes, like the custom nodes.
    - Parameters:
        + menu (GenericMenu)
            * The menu where you want to add the items to.
        + dataProperty (SerializedProperty)
            * The serialized property of the data that you want to edit.
        + allLibraries (IList))
            * The list of all libraries for the type that you want to use.
            * Example::
            
                VideoNode.AllLibraries
                
        + showNone (bool)
            * Set this to true if you want to enable the "none" option for this field in the editor.
        + pathPrefix (string)
            * The path inside the context menu where the items will be added to.

Sample code
-----------

::

    using System;
    using System.Collections.Generic;

    namespace TreeStory
    {
        [Serializable]
        [DataOf(typeof(DialogNode))]
        [DataOf(typeof(ChoiceNode))]
        public class LocationLibrary : BaseLibrary
        {
    #if UNITY_EDITOR
            public static List<LocationLibrary> AllLibraries = new List<LocationLibrary>();
    #endif

            public List<LocationData> DataList = new List<LocationData>();
        }
    }

Notes
-----

* You can add an icon for your custom library.

    - The name of the icon must be "<Class name>Icon" (without spaces).

        + Example::
        
            CustomLibraryIcon

    - Place the icon inside the location::

        <Any path including root>/Resources/Sprites/LibraryIcons/
