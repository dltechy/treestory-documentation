Creating an Editor for Your Custom Library
==========================================

Template
--------

::

    using UnityEditor;
    using UnityEngine;

    // Add using statements here as necessary.

    // You can also use your own namespace, just be careful of C#'s namespace rules.
    namespace TreeStory
    {
        // The type that you place here is the class of the library that you created in the previous section.
        [CustomEditor(typeof(/* CustomLibrary */)), CanEditMultipleObjects]
        public class /* CustomLibraryEditor */ : BaseLibraryEditor
        {
            // Add variables for your class here.
            
            // Add methods for your class here.
        }
    }

Overridable methods
-------------------

* public virtual void OnEnable
    - The usual OnEnable method of Unity.
    - If you override this method, make sure to call the "**base.OnEnable()**" method inside it.
* protected virtual float ReorderableListHandler
    - The method used to process data while drawing the reorderable list.
    - It is also used to create additional editor components beside the element's data name.
    - Parameters:
        + rect (Rect)
            * The rect of the current element inside the reorderable list.
        + index (int)
            * The index of the current element inside the reorderable list.
        + serializedElement (SerializedObject)
            * The serialized object of the current reorderable object item.
    - Returns
        + The total width of all extra properties.
* protected virtual bool CustomElementMenuItems
    - Adds menu items on each element's context menu.
    - Parameters:
        + menu (GenericMenu)
            * The generic menu for each element.
        + serializedElement (SerializedObject)
            * The serialized object of the current reorderable object item.
    - Returns:
        + True if there are custom menu items, false otherwise.
* protected virtual void OnElementInspectorGUI
    - The method used to render additional inspector elements when a reorderable list element has been selected.
    - Inspector elements that are controlled by this method will appear below the library data list.
    - Do not use the "OnInspectorGUI" method, unless you know what you are doing, as it contains custom TreeStory scripts.
    - Instead use this method like you would the OnInspectorGUI method.
    - Parameters:
        + serializedElement (SerializedObject)
            * The serialized object of the current reorderable object item.
* protected virtual void CreateSearchBar
    - Create a search bar for the library elements above the inspector window.
* public virtual void OnSearchChanged
    - Callback method that is called whenever the current search has changed.
* protected virtual bool IsSearching
    - Check if a search is being performed.
    - Returns:
        + True if a search is being performed, false otherwise.
* protected virtual bool IsElementSearched
    - Check if the specified element is part of the search results.
    - Parameters:
        + element (ScriptableObject)
            * The element to check.
    - Returns:
        + True if the element is part of the search results, false otherwise.

Usable methods
--------------

* protected string EnsureUniqueElementName
    - Ensures that the element's name is unique within the library by appending indices to the end of the name.
    - Parameters:
        + elementName (string)
            * The name of the element to check for name uniqueness.
    - Returns:
        + The unique name of the element.
* protected ScriptableObject AddData
    - Adds a new TreeStory data to the library.
    - Parameters:
        + typeString (string)
            * The type of data to add without the word "Data". (e.g. For "BaseData" pass only the string "Base".)
    - Returns:
        + The newly created data.
* protected void DeleteData
    - Deletes a TreeStory data from the library.
    - Parameters:
        + index (int)
            * The index of the data to be deleted.
* You can also use all usable methods of the custom library from the previous section since they are static.

Sample code
-----------

::

    using UnityEditor;
    using UnityEngine;

    namespace TreeStory
    {
        [CustomEditor(typeof(LocationLibrary)), CanEditMultipleObjects]
        public class LocationLibraryEditor : BaseLibraryEditor
        {
            public override void OnEnable()
            {
                base.OnEnable();

                dataRList.elementHeight = 72.0f;
            }

            protected override float ReorderableListHandler(Rect rect, int index, SerializedObject serializedElement)
            {
                Rect spriteRect = rect;
                spriteRect.height = 68.0f;
                spriteRect.width = 68.0f;
                spriteRect.x = rect.xMax - spriteRect.width;

                var backgroundProperty = serializedElement.FindProperty("Background");
                backgroundProperty.objectReferenceValue = EditorGUI.ObjectField(spriteRect, backgroundProperty.objectReferenceValue, typeof(Sprite), false);

                return spriteRect.width;
            }
        }
    }
