Creating a Custom Node
======================

Template
--------

::

    using System;

    // Add using statements here as necessary.

    #if UNITY_EDITOR
    using UnityEditor;  // Only add this line if necessary.
    using UnityEngine;
    #endif

    // You can also use your own namespace, just be careful of C#'s namespace rules.
    namespace TreeStory
    {
        [Serializable]
        // Add more attributes here.
        public class /* CustomNode */ : BaseNode
        {
    #if UNITY_EDITOR
            static Texture2D nodeIconTexture;
    #endif

            // Add variables for your class here.

    #if UNITY_EDITOR
            public override void OnEnable()
            {
                base.OnEnable();
                Color = /* Node's background color */
            }

            public override void DrawPreviewText(int nodeIndex)
            {
                DrawNodeIcon(ref nodeIconTexture, textStyle.normal.textColor);

                float textOffset = 0.0f;
                if (nodeIconTexture != null)
                    textOffset = nodeIconWidth + EditorGUIUtility.standardVerticalSpacing;

                base.DrawPreviewText(nodeIndex);

                // Add scripts to render your custom node's preview here.
            }

            // Add method overrides for your class here.
    #endif
            
            // Add regular methods for your class here.

            // To be able to use the 2 Accept methods below, you must first create extension methods for each.

            // Extend the Accept method of the "INodeVisitor" interface to be able to override this method.
            public override void Accept(INodeVisitor visitor)
            {
                visitor.Visit(this);
            }

            // Extend the Accept method of the "INodeTester" interface to be able to override this method.
            public override bool Accept(INodeTester tester)
            {
                return tester.Test(this);
            }
        }
    }

Usable attributes
-----------------

* NodeSort
    - You can add this attribute to control the order where this node appears in menus inside the editor.
* DataTypes
    - You can add this attribute to link TreeStory game data types to your node.
    - These data types are used for the data list property of this node.
    - This attribute does the exact same function as the "**DataOf**" attribute for the library classes.

        + You can use either, but they both exist in case you can't add attributes to one or the other.
* ParentNodes
    - You can add this attribute to enable a selected type of parent node to accept multiple amounts of this node as its children.

Overridable methods
-------------------

* public virtual void OnEnable
    - The usual OnEnable method of Unity.
    - If you override this method, make sure to call the "**base.OnEnable()**" method inside it.
* public virtual void InitializeNodeWithParent
    - Initializes the current node with its parent node.
    - Example usage: Copy data from the parent node to this node.
    - Parameters:
        + parentNode (BaseNode)
            * The node to set as this node's parent.
* public virtual void DrawBackground
    - Draws the background texture of the node.
* public virtual void DrawPreviewText
    - Draws the preview text of the node.
    - Parameters:
        + nodeIndex (int)
            * The index of the node inside the node library.
* public virtual bool CustomMenuItems
    - Adds menu items on the node's context menu.
    - Parameters:
        + menu (GenericMenu)
            * The context menu of the node.
    - Returns:
        + True if there are custom menu items, false otherwise.

Usable methods
--------------

* protected void DrawNodeIcon
    - Draws the node icon texture at the left side of the node.
    - This will store the image in the nodeIconTexture variable that is passed as a parameter.
    - Parameters:
        + nodeIconTexture (ref Texture2D)
            * The variable that stores the node icon texture.
            * Pass the static nodeIconTexture variable here.
        + iconTint (Color)
            * The color multiplier for the node icon texture.
* public string FormatString
    - Adds rich text xml tags to a string.
    - Parameters:
        + originalString (string)
            * The string to format.
        + isBold (bool)
            * Set this to true to make the text bold.
            * You can leave this blank if you don't need to make the text bold AND if you don't need the last 2 parameters.
        + isItalicized (bool)
            * Set this to true to make the text italicized.
            * You can leave this blank if you don't need to make the text italicized AND if you don't need the last parameter.
        + size (int)
            * The size of the text.
            * Leave this blank or set this to -1 to keep the default text size.
    - Returns:
        + The rich text formatted string.
* protected void CreatePreviewLabelField
    - Creates a label which aligns the preview text using the center anchor if it fits the node area.
    - If the text does not fit the node area, then it will be aligned at the left of the specified anchor.
    - Parameters:
        + centerTextAnchor (TextAnchor)
            * The vertical position of the text.
            * Always set its horizontal position to center.
                - **Example:** If you want to set the text's vertical position to the top, then use the "UpperCenter" value.
        + previewText (string)
            * The preview text to output.
        + textOffset (float)
            * The horizontal offset from the left used to render the text.
* public string[] DataToStringParams
    - Converts data from the node's data list property to parameters used for the string.Format() method.
    - Parameters:
        + defaultColor (Color?)
            * The default color used for the text in-game.
            * Set this to null if you don't want to add color tags to the string parameter.
            * This value is used to prevent adding the color xml tag unnecessarily.
* You can also use all usable methods of the custom library from the previous section since they are static.

Sample code
-----------

::

    using System;

    #if UNITY_EDITOR
    using UnityEditor;
    using UnityEngine;
    #endif

    namespace TreeStory
    {
        [Serializable]
        [NodeSort(9499)]
        public class LocationNode : BaseNode
        {
    #if UNITY_EDITOR
            static Texture2D nodeIconTexture;
    #endif

            public enum TransitionTypes { NoTransition, Fade }

            public LocationData Location;
            public TransitionTypes Transition = TransitionTypes.Fade;
            public bool ExitActors = true;

    #if UNITY_EDITOR
            public override void OnEnable()
            {
                base.OnEnable();
                Color = new Color(0.85f, 0.7f, 0.55f);
            }

            public override void DrawPreviewText(int nodeIndex)
            {
                DrawNodeIcon(ref nodeIconTexture, textStyle.normal.textColor);

                float textOffset = 0.0f;
                if (nodeIconTexture != null)
                    textOffset = nodeIconWidth + EditorGUIUtility.standardVerticalSpacing;

                base.DrawPreviewText(nodeIndex);

                string previewText = "";
                if (Location != null)
                    previewText = Location.name;

                string headerText = ObjectNames.NicifyVariableName(Transition.ToString());
                headerText = FormatString(headerText, true);

                CreatePreviewLabelField(TextAnchor.UpperCenter, headerText, textOffset);
                CreatePreviewLabelField(TextAnchor.LowerCenter, previewText, textOffset);
            }

            public override bool CustomMenuItems(GenericMenu menu)
            {
                SerializedObject serializedObjects = new SerializedObject(Selection.objects);
                SerializedProperty locationProperty = serializedObjects.FindProperty("Location");

                BaseLibrary.AddMenuItems(menu, locationProperty, LocationLibrary.AllLibraries, true, "Set Location/");

                return true;
            }
    #endif

            public override void Accept(INodeVisitor visitor)
            {
                visitor.Visit(this);
            }

            public override bool Accept(INodeTester tester)
            {
                return tester.Test(this);
            }
        }
    }

Sample method extensions
------------------------

::

    namespace TreeStory
    {
        public interface INodeProcessor : INodeVisitor
        {
            void Visit(ActorNode node);
            void Visit(LocationNode node);
        }

        public static class NodeProcessorExtension
        {
            public static void Visit(this INodeVisitor visitor, ActorNode node)
            {
                (visitor as INodeProcessor).Visit(node);
            }

            public static void Visit(this INodeVisitor visitor, LocationNode node)
            {
                (visitor as INodeProcessor).Visit(node);
            }
        }
    }

::

    namespace TreeStory
    {
        public interface INodeFinishTester : INodeTester
        {
            bool Test(ActorNode node);
            bool Test(LocationNode node);
        }

        public static class NodeFinishTesterExtension
        {
            public static bool Test(this INodeTester tester, ActorNode node)
            {
                return (tester as INodeFinishTester).Test(node);
            }

            public static bool Test(this INodeTester tester, LocationNode node)
            {
                return (tester as INodeFinishTester).Test(node);
            }
        }
    }

Notes
-----

* You can add an icon for your custom node.

    - The name of the icon must be "<Class name>Icon" (without spaces).
    
        + Example::
        
            CustomNodeIcon

    - Place the icon inside the location::

        <Any path including root>/Resources/Sprites/NodeIcons/

    - **Tip:** Make your icon image's color white to enable changing its color through script.
