Creating a Custom Event Handler
===============================

Template
--------

::

    using UnityEngine;

    // Add using statements here as necessary.

    // You can also use your own namespace, just be careful of C#'s namespace rules.
    namespace TreeStory
    {
        public class /* CustomEventHandler */ : EventHandler
        {
            // Add variables for your class here.

            // The virtual keyword is optional but recommended in case you want to extend it in derived classes.
            public virtual void Update()
            {
                // Place your custom script here on how the "**IsTriggered**" variable becomes true.
                // It automatically returns to false after one frame.
            }
            
            // Add methods for your class here.
        }
    }

Overridable methods
-------------------

* public virtual void Awake
    - The usual Awake method of Unity.
    - Its base method only initializes the "**IsTriggered**" variable to false.
* public virtual void LateUpdate
    - The usual LateUpdate method of Unity.
    - Its base method only resets the "**IsTriggered**" variable to false.

Sample code
-----------

::

    using UnityEngine;

    namespace TreeStory
    {
        public class KeyboardHandler : EventHandler
        {
            [Tooltip("The keyboard key that will trigger the event.")]
            public KeyCode KeyCode = KeyCode.Escape;

            public virtual void Update()
            {
                if (Input.GetKeyUp(KeyCode))
                    IsTriggered = true;
            }
        }
    }
