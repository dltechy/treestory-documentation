Creating a Custom Game Data
===========================

Template
--------

::

    using System;
    using UnityEngine;

    // Add using statements here as necessary.

    // You can also use your own namespace, just be careful of C#'s namespace rules.
    namespace TreeStory
    {
        [Serializable]
        public class /* CustomData */ : ScriptableObject
        {
            // Add variables for your class here.

            public void OnEnable()
            {
                hideFlags = HideFlags.HideInHierarchy;
            }

            // Add methods for your class here.
        }
    }

Sample code
-----------

::

    using System;
    using UnityEngine;

    namespace TreeStory
    {
        [Serializable]
        public class LocationData : ScriptableObject
        {
            public Sprite Background;

            public void OnEnable()
            {
                hideFlags = HideFlags.HideInHierarchy;
            }
        }
    }
