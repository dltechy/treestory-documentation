Creating an Editor for Your Custom Event Handler
================================================

Template
--------

::

    using UnityEditor;

    // Add using statements here as necessary.

    // You can also use your own namespace, just be careful of C#'s namespace rules.
    namespace TreeStory
    {
        // The type that you place here is the class of the event handler that you created in the previous section.
        [CustomEditor(typeof(/* CustomEventHandler */)), CanEditMultipleObjects]
        public class /* CustomEventHandlerEditor */ : EventHandlerEditor
        {
            // Add variables for your class here.

            // Add methods for your class here.
        }
    }

Overridable methods
-------------------

* public virtual void OnEnable
    - The usual OnEnable method of Unity.
    - If you override this method, make sure to call the "**base.OnEnable()**" method inside it.

Sample code
-----------

::

    using UnityEditor;

    namespace TreeStory
    {
        [CustomEditor(typeof(KeyboardHandler)), CanEditMultipleObjects]
        public class KeyboardHandlerEditor : EventHandlerEditor
        {
            SerializedProperty keyCodeProperty;

            public override void OnEnable()
            {
                keyCodeProperty = serializedObject.FindProperty("KeyCode");

                base.OnEnable();
            }

            public override void OnInspectorGUI()
            {
                serializedObject.Update();

                EditorGUILayout.PropertyField(keyCodeProperty);

                EditorGUILayout.Space();

                nodeLibrariesRList.DoLayoutList();

                serializedObject.ApplyModifiedProperties();
            }
        }
    }
