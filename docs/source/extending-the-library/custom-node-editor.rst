Creating an Editor for Your Custom Node
=======================================

Template
--------

::

    using UnityEditor;
    using UnityEngine;

    // Add using statements here as necessary.

    // You can also use your own namespace, just be careful of C#'s namespace rules.
    namespace TreeStory
    {
        // The type that you place here is the class of the node that you created in the previous section.
        [CustomEditor(typeof(/* CustomNode */)), CanEditMultipleObjects]
        public class /* CustomNodeEditor */ : BaseNodeEditor
        {
            // Add variables for your class here.
            
            // Add methods for your class here.
        }
    }

Overridable methods
-------------------

* public virtual void OnEnable
    - The usual OnEnable method of Unity.
    - If you override this method, make sure to call the "**base.OnEnable()**" method inside it.
* protected virtual void OnDerivedInspectorGUI
    - The method used to render inspector elements.
    - Do not use the "OnInspectorGUI" method, unless you know what you are doing, as it contains custom TreeStory scripts.
    - Instead use this method like you would the OnInspectorGUI method.

Usable methods
--------------

* protected ReorderableList CreateDataRList
    - Creates a reorderable list for a node's data list.
    - Parameters:
        + nodeType (Type)
            * The type of the node which contains the data list.
        + dataListProperty (SerializedProperty)
            * The serialized property of the data list.
        + headerText (string)
            * The text displayed on the reorderable list's header.
            * Keep this empty to use the default value for the header text.
    - Returns:
        + The reorderable list for a node's data list.
* protected void CreateWaitToFinishField
    - Use this method inside the "OnDerivedInspectorGUI" method (preferably at the start of the method) if your node uses the "wait to finish" property.
* protected static void CreateTextAreaField
    - Use this method inside the "OnDerivedInspectorGUI" method to add a text area field with a prefix label to the editor.
    - Parameters:
        + nodeTextProperty (SerializedProperty)
            * The serialized property of the text that you want to edit.
        + tooltip (string)
            * The tooltip that will be displayed when you hover your mouse over the property inside Unity's inspector window.
            * Leave this blank or set it to an empty string to not display a tooltip.
* You can also use all usable methods of the custom library from the previous section since they are static.

Sample code
-----------

::

    using UnityEditor;
    using UnityEngine;

    namespace TreeStory
    {
        [CustomEditor(typeof(LocationNode)), CanEditMultipleObjects]
        public class LocationNodeEditor : BaseNodeEditor
        {
            SerializedProperty locationProperty;
            SerializedProperty transitionProperty;
            SerializedProperty exitActorsProperty;

            public override void OnEnable()
            {
                base.OnEnable();

                locationProperty = serializedObject.FindProperty("Location");
                transitionProperty = serializedObject.FindProperty("Transition");
                exitActorsProperty = serializedObject.FindProperty("ExitActors");
            }

            protected override void OnDerivedInspectorGUI()
            {
                CreateWaitToFinishField();

                BaseLibrary.CreateDataDropdown(locationProperty, LocationLibrary.AllLibraries, true, "The location associated with this node.");

                GUIContent transitionLabel = new GUIContent(transitionProperty.displayName, "The transition effect used to transfer locations.");
                EditorGUILayout.PropertyField(transitionProperty, transitionLabel);

                GUIContent exitActorsLabel = new GUIContent(exitActorsProperty.displayName, "If checked, all actors currently in-game will exit.");
                EditorGUILayout.PropertyField(exitActorsProperty, exitActorsLabel);
            }
        }
    }
