Creating a Simple Visual Novel
==============================

* Creating a visual novel using TreeStory is very simple.
* You control how your game works by creating node libraries.
* The next step is figuring out how to present it to your users.
* TreeStory makes that job easier for you by providing you with prefabs as shown in the previous section.
* To make it even easier, TreeStory also provides you with template prefabs.

    - These templates does the job of setting the other prefabs for you.

Setting Up the Base for Each Scene
----------------------------------

* Set up the base for all of the scenes for your game using the following steps and TreeStory will make them work seamlessly with each other.

1. Delete the default camera from the scene.

.. figure:: /_images/template-prefabs/default-camera.png
    :align: center
    :alt: default camera

    The Default Camera
    
2. Drag the "TreeStoryVN" prefab into the scene.

    * Location::

        TreeStory/Prefabs/Templates/TreeStoryVN
        
.. figure:: /_images/template-prefabs/treestoryvn-template-prefab.png
    :align: center
    :alt: treestoryvn template prefab

    The TreeStoryVN Template Prefab

3. Select the "TreeStoryVN" game object from the hierarchy (The root game object).

    * You should see a "Node Library" property field in the inspector window.

.. figure:: /_images/template-prefabs/treestoryvn-gameobject-inspector.png
    :align: center
    :alt: treestoryvn gameobject inspector

    The TreeStoryVN GameObject Inspector
    
4. Drag your main node library asset into this field.

    * Alternatively, you can click on the circle on the right of this field.

* **Note:** You can add buttons to your game by using the "button" prefab.

    - A ButtonLayer object is provided as a parent object for your buttons.

.. toctree::
    :caption: Next Steps
    :maxdepth: 1

    creating-a-simple-visual-novel/creating-the-main-game
    creating-a-simple-visual-novel/creating-a-menu
