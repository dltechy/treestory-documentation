The EventSystem Prefab
======================

* Prefab location::

    TreeStory/Prefabs/Singletons/EventSystem

* Script location::

    TreeStory/TreeStoryVN/GUI/Singletons/EventSystem.cs

* This is basically Unity's event system object.
* This prefab is a singleton object.

    - Only one EventSystem object exists in the game at all times.
    - If a new scene loads with a new EventSystem object, then that object gets destroyed.
    - It doesn't get destroyed by a scene change.

* No customizable script properties.
