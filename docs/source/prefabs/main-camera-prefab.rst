The MainCamera Prefab
=====================

* Prefab location::

    TreeStory/Prefabs/Singletons/MainCamera

* Script location::

    TreeStory/TreeStoryVN/GUI/Singletons/MainCamera.cs

* This is basically Unity's camera object.
* This prefab is a singleton object.

    - Only one MainCamera object exists in the game at all times.
    - If a new scene loads with a new MainCamera object, then that object gets destroyed.
    - It doesn't get destroyed by a scene change.

* If your game needs more than one camera, then don't use this prefab.
* No customizable script properties.
