The NameBox Prefab
==================

* Prefab location::

    TreeStory/Prefabs/Dialog/NameBox

* Script location::

    TreeStory/TreeStoryVN/GUI/Dialog/NameBox.cs

* Controlled by:
    - Dialog nodes
* Shows the name of the actor associated (speaking, thinking, etc.) with the currently shown dialog, if available.
* Hidden if the dialog does not have an associated actor.

Customizable script properties
------------------------------

* Source image (Image script)
    - The background image used by the name box.
    - Useful to add borders to your name box.
