The FadeImage Prefab
====================

* Prefab location::

    TreeStory/Prefabs/Overlay/FadeImage

* Script location::

    TreeStory/TreeStoryVN/GUI/Overlay/FadeImage.cs

* Controlled by:
    - Start nodes
    - Location nodes
    - Scene nodes
* Enables the fade transition effect for scene and location changes.

Customizable script properties
------------------------------

* Fade speed
    - The percentage per frame (0.0 to 1.0) at which the game fades in/out every time the scene or location changes.
    - This is only applicable if the scene or location node's transition is set to fade.
