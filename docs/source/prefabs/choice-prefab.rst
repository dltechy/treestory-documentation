The Choice Prefab
=================

* Prefab location::

    TreeStory/Prefabs/Choices/Choice

* Script location::

    TreeStory/TreeStoryVN/GUI/Choices/ChoiceButton.cs

* Controlled by:
    - Choice nodes
* Handles a single choice in-game.
* Needs to be placed as a child to the choice layer game object.

Customizable script properties
------------------------------

* Is enabled
    - If checked, the button can be selected.
    - Controlled by the ChoiceLayer prefab.
* Is override enabled
    - If checked, input events won't affect the highlight and pressed state of the button.
    - Instead an external script can control these states.
    - Controlled by the ChoiceLayer prefab.
* Normal image
    - The background image used for the button normally.
* Highlight image
    - The background image used for the button when it is highlighted.
* Pressed image
    - The background image used for the button when it is pressed.
* Normal color
    - The color used for the background image of the button normally.
* Highlight color
    - The color used for the background image of the button when it is highlighted.
* Pressed color
    - The color used for the background image of the button when it is pressed.
* Highlight sound
    - The sound effect used when the user highlights then button.
* Select sound
    - The sound effect used when the user selects the button.
* Node libraries
    - The node libraries to load inside the game when the choice is pressed.
