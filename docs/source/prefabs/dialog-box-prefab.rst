The DialogBox Prefab
====================

* Prefab location::

    TreeStory/Prefabs/Dialog/DialogBox

* Script location::

    TreeStory/TreeStoryVN/GUI/Dialog/DialogBox.cs

* Controlled by:
    - Dialog nodes
* Shows the current dialog being spoken.

Customizable script properties
------------------------------

* Character speed
    - The time in seconds that it takes to show each text character to the screen.
* Source image (Image script)
    - The background image used by the dialog box.
    - Useful to add borders to your dialog box.
