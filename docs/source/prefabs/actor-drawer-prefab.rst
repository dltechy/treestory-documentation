The ActorDrawer Prefab
======================

* Prefab location::

    TreeStory/Prefabs/Actors/ActorDrawer

* Script location::

    TreeStory/TreeStoryVN/GUI/Actors/ActorDrawer.cs

* Controlled by:
    - Actor nodes
    - Location nodes

        + Only able to exit all actors controlled by this prefab.

* Handles the behavior of all actors in the scene.

Customizable script properties
------------------------------

* Actor image prefab
    - A prefab which contains an image component and an actor image script.
    - The actor image script controls the behavior of a single actor.
