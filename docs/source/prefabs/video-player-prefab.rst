The VideoPlayer Prefab
======================

* Prefab location::

    TreeStory/Prefabs/Overlay/VideoPlayer

* Script location::

    TreeStory/TreeStoryVN/GUI/Overlay/VideoPlayer.cs

* Controlled by:
    - Video nodes
* Plays the cut scenes for your game.

Customizable script properties
------------------------------

* Background color
    - The color used to fill the empty spaces of the screen if the video is smaller than the user's screen.
    - Set this color to transparent (alpha = 0) if you don't want to fill the empty spaces of the screen.
* Aspect mode
    - The type of stretching that will happen to your video depending on the screen size of the user.
    - This property overrides the aspect mode property of the aspect ratio fitter component.
    - Values:
        + None
            * The video stretches to the full screen size.
        + Width controls height
            * The video's width will stretch to the full width of the screen.
            * Its height will change to match the original aspect ratio of the image.
        + Height controls width
            * The video's height will stretch to the full height of the screen.
            * Its width will change to match the original aspect ratio of the image.
        + Fit in parent
            * The video will fit itself to the size of the user's screen while keeping its original aspect ratio.
        + Envelope parent
            * The video will fill the size of the user's screen while keeping its original aspect ratio.
