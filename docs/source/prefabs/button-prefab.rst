The Button Prefab
=================

* Prefab location::

    TreeStory/Prefabs/EventHandlers/Button

* Script location::

    TreeStory/TreeStoryVN/GUI/EventHandlers/Button.cs

* A button that loads node libraries to the game whenever it is pressed.
* You must place instances of this prefab inside a parent object which contains a "TreeStoryVN" script component.

Customizable script properties
------------------------------

* Is enabled
    - If checked, the button can be selected.
* Is override enabled
    - If checked, input events won't affect the highlight and pressed state of the button.
    - Instead an external script can control these states.
* Normal image
    - The background image used for the button normally.
* Highlight image
    - The background image used for the button when it is highlighted.
* Pressed image
    - The background image used for the button when it is pressed.
* Normal color
    - The color used for the background image of the button normally.
* Highlight color
    - The color used for the background image of the button when it is highlighted.
* Pressed color
    - The color used for the background image of the button when it is pressed.
* Highlight sound
    - The sound effect used when the user highlights then button.
* Select sound
    - The sound effect used when the user selects the button.
* Node libraries
    - The node libraries to load inside the game when the button is pressed.
