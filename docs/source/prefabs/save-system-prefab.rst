The SaveSystem Prefab
=====================

* Prefab location::

    TreeStory/Prefabs/SaveSystem/SaveSystem

* Script location::

    TreeStory/TreeStorySaveSystem/Scripts/SaveSystem.cs

* This prefab is actually part of the TreeStorySaveSystem package.
* It handles saving and loading of data for your TreeStory game.
* This prefab is a singleton object.

    - Only one SaveSystem object exists in the game at all times.
    - If a new scene loads with a new SaveSystem object, then that object gets destroyed.
    - It doesn't get destroyed by a scene change.

* No customizable script properties.
