The KeyboardHandler Prefab
==========================

* Prefab location::

    TreeStory/Prefabs/EventHandlers/KeyboardHandler

* Script location::

    TreeStory/TreeStoryVN/GUI/EventHandlers/KeyboardHandler.cs

* Loads node libraries to the game whenever the specified keyboard key is pressed.
* You must place instances of this prefab inside a parent object which contains a "TreeStoryVN" script component.

Customizable script properties
------------------------------

* Key code
    - The keyboard key that will trigger the loading of the node libraries.
* Node libraries
    - The node libraries to load inside the game when the keyboard key is pressed.
