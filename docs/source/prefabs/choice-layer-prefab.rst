The ChoiceLayer Prefab
======================

* Prefab location::

    TreeStory/Prefabs/Choices/ChoiceLayer

* Script location::

    TreeStory/TreeStoryVN/GUI/Choices/ChoiceLayer.cs

* Controlled by:
    - Dialog nodes
    - Choice nodes
* Handles the selection of choices in-game.
* You can set audio clips to this game object to add sound effects every time a choice is highlighted and/or selected.

Customizable script properties
------------------------------

* Highlight sound
    - The sound effect used when the user highlights a choice.
    - If this property is not empty, then it overrides the highlight sound property of all its child choices.
* Select sound
    - The sound effect used when the user selects a choice.
    - If this property is not empty, then it overrides the select sound property of all its child choices.
* Choice delay
    - The time in seconds that the choice blinks when selected before moving on to the next node.
    - Set this to 0 if you want your game to immediately continue to the next node when selecting a choice.
* Source image (Image script)
    - The background image used by the choice layer.
    - Useful to add borders to your choices.
