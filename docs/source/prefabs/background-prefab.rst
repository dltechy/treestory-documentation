The Background Prefab
=====================

* Prefab location::

    TreeStory/Prefabs/Background/Background

* Script location::

    TreeStory/TreeStoryVN/GUI/Background/Background.cs

* Controlled by:
    - Location nodes
* Shows the background image of the locations in your game.

Customizable script properties
------------------------------

* Aspect mode (Aspect ratio fitter script)
    - The type of stretching that will happen to your background image depending on the screen size of the user.
    - Values:
        + None
            * The background image stretches to the full screen size.
        + Width controls height
            * The background image's width will stretch to the full width of the screen.
            * Its height will change to match the original aspect ratio of the image.
        + Height controls width
            * The background image's height will stretch to the full height of the screen.
            * Its width will change to match the original aspect ratio of the image.
        + Fit in parent
            * The background image will fit itself to the size of the user's screen while keeping its original aspect ratio.
        + Envelope parent
            * The background image will fill the size of the user's screen while keeping its original aspect ratio.
