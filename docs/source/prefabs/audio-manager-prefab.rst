The AudioManager Prefab
=======================

* Prefab location::

    TreeStory/Prefabs/Audio/AudioManager

* Script location::

    TreeStory/TreeStoryVN/GUI/Audio/AudioManager.cs

* Controlled by:
    - Audio nodes
    - Video nodes

        + Only able to stop all audio controlled by this prefab.

* Handles both the background music and all sound effects.
* This prefab is a singleton object.

    - Only one audio manager object exists in the game at all times.
    - If a new scene loads with a new AudioManager object, then that object gets destroyed.
    - It doesn't get destroyed by a scene change.

Customizable script properties
------------------------------

* Audio source prefab
    - A prefab which contains an audio source component.
* Fade speed
    - The percentage per frame (0.0 to 1.0) at which the game's background music fades to the value set by an audio node's "BGM Fade" property.
