The MouseButtonHandler Prefab
=============================

* Prefab location::

    TreeStory/Prefabs/EventHandlers/MouseButtonHandler

* Script location::

    TreeStory/TreeStoryVN/GUI/EventHandlers/MouseButtonHandler.cs

* Loads node libraries to the game whenever the specified mouse button is pressed.
* You must place instances of this prefab inside a parent object which contains a "TreeStoryVN" script component.

Customizable script properties
------------------------------

* Button
    - The mouse button that will trigger the loading of the node libraries.
* Node libraries
    - The node libraries to load inside the game when the mouse button is pressed.
