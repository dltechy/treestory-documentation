Editing the Choice Node
=======================

.. figure:: /_images/nodes/choice-node.png
    :align: center
    :alt: choice node

    Choice Node

* The choice node control the choices that are shown in-game.
* It can only be targeted by a dialog node.
* To enable it in-game, you need to have enough buttons pre-made in the game's GUI to accommodate for the number of choice nodes.

    - This will be discussed later in the "Creating the Game UI" section.
    - **Quick example:** If you have 3 choice nodes for a dialog node, you must have 3 or more choice buttons pre-made in the game's GUI.

Choice node properties
----------------------

* Text
    - The text displayed for the choice in-game.
    - TreeStory's choices accepts Unity's rich text formatting.

        + Please refer to Unity's documentation on how to use rich text formatting.

    - It also accepts C# style text formatting with its parameters taken from the data list discussed below.

        + **Example:** "Hello my name is {0}. I live in {1}."

            * Where {0} and {1} are the first and second entry in the data list, respectively.

        + Be careful about your indices as adding, removing, or moving your data might alter their index.

            * When this happens, you must adjust the indices that you have already used accordingly.

    - A preview for the formatted choice text can be seen at the bottom of the choice node's inspector window.

* Data list
    - A list of data attached to the current choice node used to make its text dynamic.
    - Accepted data:
        + Actor data
        + Location data
        + Variable data
