Editing the Audio Node
======================

.. figure:: /_images/nodes/audio-node.png
    :align: center
    :alt: audio node

    Audio Node

* The audio node controls the behavior of audio in-game.
* The audio data that this node uses should be first made inside the audio library.

Audio node properties
---------------------

* **Wait to finish**
* Audio
    - The audio file that will be played.
    - Set this to none to only control the background music.
* Is BGM
    - If checked, the associated audio will be set as the background music of the game.
    - Only one BGM is allowed to play at a time.
    - If you want to stop the background music from playing, set the audio to none and check this property.
* Change BGM volume
    - If checked, the volume of the game's "background music" will be set to the selected value.
* Target BGM volume
    - The amount of volume that the game's "background music" will be set to.
    - This option will only appear if the "Change BGM Volume" property above is checked.
    - This property can be set from 0.0 to 1.0
    - **Note:** If this property is set to 0.0, the background music will still be playing, the user just won't hear it.
