Editing the Location Node
=========================

**Note:** This section contains data relevant only to the provided TreeStoryVN package.

.. figure:: /_images/nodes/location-node.png
    :align: center
    :alt: location node

    Location Node

* The location node controls the behavior of locations in-game.
* The locations that this node uses should be first made inside the location library.
* For the TreeStoryVN package, the locations is represented as the background image in-game.

Location node properties
------------------------

* **Wait to finish**
* Location
    - The location associated with this node.
    - You can also set this property by right-clicking on the node inside the TreeStory editor and selecting "Set Location".
* Transition
    - The transition effect used to transfer locations.
    - Values:
        + No transition
            * Don't use transitions while transferring locations.
        + Fade
            * Starts with the screen black and fading in.
* Exit actors
    - If CHECKED, all actors currently in-game will exit.
