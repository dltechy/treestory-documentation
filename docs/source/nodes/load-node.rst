Editing the Load Node
=====================

.. figure:: /_images/nodes/load-node.png
    :align: center
    :alt: load node

    Load Node

* The load node loads game data from a file.
* The save files are created using the save node.
* This node automatically loads the main scene where the save file was created.

Load node properties
--------------------

* File name
    - The file where the save data are stored.
