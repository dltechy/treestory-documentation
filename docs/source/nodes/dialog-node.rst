Editing the Dialog Node
=======================

.. figure:: /_images/nodes/dialog-node.png
    :align: center
    :alt: dialog node

    Dialog Node

* The dialog node controls the dialog that is shown in-game.
* It also controls the actor's name that will be displayed with the dialog in-game.
* To enable it in-game, you need to have a dialog box pre-made in the game's GUI.

    - This will be discussed later in the "Creating the Game UI" section.

Dialog node properties
----------------------

* **Wait to finish**
* Actor Name
    - The name of the actor that is associated (speaking, thinking, etc.) with the current dialog.
    - TreeStory's actor name accepts Unity's rich text formatting.

        + Please refer to Unity's documentation on how to use rich text formatting.

    - It also accepts C# style text formatting with its parameters taken from the data list discussed below.

        + Example: "Mister {0}"

            * Where {0} is the first entry in the data list.

        + Be careful about your indices as adding, removing, or moving your data might alter their index.

            * When this happens, you must adjust the indices that you have already used accordingly.

    - It is recommended to use the data list for the actor names in case you rename any actor in the future.

        + Doing this prevents the need for you to change every single name for each dialog node, and instead just change one entry in the actor library.

    - A preview for the formatted actor name can be seen at the bottom of the dialog node's inspector window.
* Text
    - The text displayed for the dialog in-game.
    - TreeStory's dialog accepts Unity's rich text formatting.

        + Please refer to Unity's documentation on how to use rich text formatting.

    - It also accepts C# style text formatting with its parameters taken from the data list discussed below.

        + Example: "Hello my name is {0}. I live in {1}."

            * Where {0} and {1} are the first and second entry in the data list, respectively.

        + Be careful about your indices as adding, removing, or moving your data might alter their index.

            * When this happens, you must adjust the indices that you have already used accordingly.

    - A preview for the formatted dialog text can be seen at the bottom of the dialog node's inspector window.
* Data list
    - A list of data attached to the current dialog node used to make its text dynamic.
    - Accepted data:
        + Actor data
        + Location data
        + Variable data
* Choices
    - A list of text of all choice nodes which is targeted by the current dialog node.
    - These choice node texts can be edited directly from here.
    - You can rearrange the order of choices from this list by dragging them.

        + This affects how the choices are ordered in-game as well.

    - But you cannot add or remove choice nodes from this node.
* Data list (Selected choice)
    - The data list for the selected choice from the choices list.
    - Refer to the data list property of the choice node below.
