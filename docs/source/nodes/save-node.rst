Editing the Save Node
=====================

.. figure:: /_images/nodes/save-node.png
    :align: center
    :alt: save node

    Save Node

* The save node saves game data to a file.
* You can load the created data using the load node.
* The save files are located in Unity's persistent data path.

    - For windows it is located in::
    
        "C:\Users\<User Name>\AppData\LocalLow\<Company Name>\<Project Name>\Save Data\"

Save node properties
--------------------

* File name
    - The file where the save data will be stored.
