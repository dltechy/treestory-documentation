Editing the Scene Node
======================

.. figure:: /_images/nodes/scene-node.png
    :align: center
    :alt: scene node

    Scene Node

* The scene node controls the behavior of scenes in-game.
* An example for this node's use is for your game's menus.
* **Note:** Changing scenes with the "single" scene mode makes Unity forget all of its current data unless you use specific scripts.
    - TreeStory provides you with prefabs to keep some data for saving and loading data to your game.

Scene node properties
---------------------

* Scene
    - The new scene that will be loaded.
    - You can also set this property by right-clicking on the node inside the TreeStory editor and selecting "Set Scene".
* Load scene mode
    - The mode that Unity will use to load the new scene.
    - Values:
        + Single
            * Close all open scenes and load the new scene.
        + Additive
            * Open the new scene alongside all open scenes.
* Transition
    - The transition effect used to transfer scenes.
    - Values:
        + No transition
            * Don't use transitions while transferring scenes.
        + Fade
            * Starts with the screen black and fading in.
