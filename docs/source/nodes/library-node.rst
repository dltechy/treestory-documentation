Editing the Library Node
========================

.. figure:: /_images/nodes/library-node.png
    :align: center
    :alt: library node

    Library Node

* The library node makes the game continue using a different node library.
* This enables you to split your project into multiple node libraries but still use it like a single one.
* This is useful if you work in a team and each member works on a different part of the game.
* It's also useful in making your project more manageable.

Library node properties
-----------------------

* Library
    - The TreeStory node library that the game will continue on.
    - The game will finish that library before moving on to the next node, if any.
    - You can also set this property by right-clicking on the node inside the TreeStory editor and selecting "Set Library".
