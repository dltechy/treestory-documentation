Editing the Actor Node
======================

**Note:** This section contains data relevant only to the provided TreeStoryVN package.

.. figure:: /_images/nodes/actor-node.png
    :align: center
    :alt: actor node

    Actor Node

* The actor node controls the behavior of an actor in-game.
* The actors that this node uses should be first made inside the actor library.

Actor node properties
---------------------

* **Wait to finish**
* Actor
    - The actor which is associated with this node.
    - You can also set this property by right-clicking on the node inside the TreeStory editor and selecting "Set Actor".
    - The rest of the properties won't appear in the inspector until you have selected an actor.
* Event
    - The type of event that will happen with this node's associated actor.
    - Values:
        + Enter
            * The actor enters the scene.
        + Move
            * Move an already existing actor's position.
        + Sprite
            * Change an already existing actor's sprite.
        + Exit
            * Exit an already existing actor out of the scene.
    - Each event, when selected, will show different properties for the current actor node.

Enter event properties
----------------------

* Sprite
    - The sprite used by the associated actor.
    - This property cannot be empty, otherwise the game will crash.
    - If it is empty, the node's preview in the editor (the image above) will show an exclamation mark to help you identify which nodes have empty sprite properties.
* Enter from
    - Where the actor will enter from.
    - Values:
        + Pop
            * Appear immediately into the screen.
        + Fade
            * Fades slowly into the screen.
        + Left
            * Enters from the left of the screen.
        + Right
            * Enters from the right of the screen.
        + Top
            * Enters from the top of the screen.
        + Bottom
            * Enters from the bottom of the screen.
* Screen origin
    - The origin of the screen, in percent, where the target position will be based on.
* Target position
    - The target position, in pixels from the screen origin, for the actor.
* Event duration
    - How long the actor will take to move to its target position.
    - This property only appears if the "Enter from" property is NOT set to "Pop".

Move event properties
---------------------

* Move X
    - The type of the actor's horizontal movement.
    - Values:
        + None
            * The actor will NOT move horizontally.
        + Absolute
            * The actor's exact target horizontal position will be set by you.
        + Relative
            * The actor will move horizontally by the specified amount (in pixels) relative to its current position.
* Screen origin X
    - The horizontal origin of the screen, in percent, where the target position will be based on.
    - This property will only be visible if the "Move X" property is set to "Absolute".
* Target X position
    - The target horizontal position, in pixels from the horizontal screen origin, for the actor.
    - This property will only be visible if the "Move X" property is NOT set to "None".
* Move Y
    - The type of the actor's vertical movement.
    - Values:
        + None
            * The actor will NOT move vertically.
        + Absolute
            * The actor's exact target vertical position will be set by you.
        + Relative
            * The actor will move vertically by the specified amount (in pixels) relative to its current position.
* Screen origin Y
    - The vertical origin of the screen, in percent, where the target position will be based on.
    - This property will only be visible if the "Move Y" property is set to "Absolute".
* Target Y position
    - The target vertical position, in pixels from the vertical screen origin, for the actor.
    - This property will only be visible if the "Move Y" property is NOT set to "None".
* Event duration
    - How long the actor will take to move to its target position.

Sprite event properties
-----------------------

* Sprite
    - The sprite used by the associated actor. 
    - This property cannot be empty, otherwise the game will crash.
    - If it is empty, the node's preview in the editor (the image above) will show an exclamation mark to help you identify which nodes have empty sprite properties.

Exit event properties
---------------------

* Exit to
    - Where the actor will exit to.
    - Values:
        + Pop
            * Dissapear immediately from the screen.
        + Fade
            * Fades slowly out of the screen.
        + Left
            * Exits to the left of the screen.
        + Right
            * Exits to the right of the screen.
        + Top
            * Exits to the top of the screen.
        + Bottom
            * Exits to the bottom of the screen.
* Event duration
    - How long the actor will take to move to its target position.
    - This property only appears if the "Exit to" property is NOT set to "Pop".
