Editing the Video Node
======================

.. figure:: /_images/nodes/video-node.png
    :align: center
    :alt: video node

    Video Node

* The video node controls the behavior of video in-game (like cut scenes).
* The video data that this node uses should be first made inside the video library.

Video node properties
---------------------

* Is skippable
    - If CHECKED, the video will be skippable by the user using the normal dialog controls.
* Video
    - The video file that will be played.
* Stop all sounds
    - If CHECKED, all sounds that are currently playing (BGM and all sound effects) will stop.
    - **Note:** The sounds will NOT start again once the video has finished.
        + If you want to continue the BGM playback, you have to play it again using a new audio node.
