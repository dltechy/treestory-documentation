Editing the Quit Node
=====================

.. figure:: /_images/nodes/quit-node.png
    :align: center
    :alt: quit node

    Quit Node

* The quit node quits the game.

    - **Note:** Unity does not quit the game if it runs in the editor. Only in the published game.
    
* This node has no additional properties.
