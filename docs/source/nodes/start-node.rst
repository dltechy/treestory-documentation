Editing the Start Node
======================

.. figure:: /_images/nodes/start-node.png
    :align: center
    :alt: start node

    Start Node

* The start node is a special type of node.
* There is only one of it in every node library.
* This node cannot be deleted.
* This is where your game starts reading from your node library.

Start node properties
---------------------

* Start faded
    - If CHECKED, the game will fade in from black.
